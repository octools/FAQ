# FAQ OCTOOLS



## Objectifs

* Répondre aux questions fréqemment posées ;
* Permettre à tout le monde d'ajouter ce qui lui semble important ;
* Offrir une base de connaissances aux nouveaux étudiants ;
* Créer une veille collective sur les outils à connaitre.



## Comment ajouter une question ?

* Pour les questions générales, ajoutez un fichier dans ce dossier (otools/fac) à l'aide du bouton `+` situé ci-dessus. Pour les questions relatives à un thème correspondant à l'un des dossiers, postez la question dans le dossier ;
* Utilisez un nom explicite afin que l'on comprenne facilement de quoi il traite ;
* Créez le fichier au format .md (utilisant donc le formatage Markdown) ;
* Rédigez la question et la réponse ;
* Rédigez un message de commit en raport avec votre ajout ;
* Utilisez la branche "master' ;
* Envoyez votre fichier pour qu'il soit validé.



## Comment modifier une réponse ?

* Utilisez le bouton 'Edit' situé en haut à droite dans la fenetre de visualisation du fichier à modifier ;
* Effectuez votre modification ;
* Rédigez un message de commit en raport avec votre modification ;
* Utilisez la branche "master' ;
* Envoyez le fichier pour qu'il soit validé.