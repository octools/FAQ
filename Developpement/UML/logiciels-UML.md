# Quels sont les logiciels UML recommandés ?

* [Bouml](http://bouml.fr/index_fr.html)
* [staruml](http://staruml.io/) (Payant - conditions d'essai à indiquer)
* [visual paradigm](https://www.visual-paradigm.com/) (Payant - 30 jours d'essai)
* [draw.io](http://www.draw.io) (Outil en ligne)
* [Dia](http://dia-installer.de/index.html.fr)